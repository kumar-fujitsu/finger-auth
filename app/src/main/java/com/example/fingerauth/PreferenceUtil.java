package com.example.fingerauth;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceUtil {

    private static PreferenceUtil instance;
    private static SharedPreferences sharedPreferences;

    public static PreferenceUtil getInstance(Context context){
        if (instance == null){
            instance = new PreferenceUtil();
            sharedPreferences = context.getSharedPreferences(ConstantUtil.APP_PREFS,0);
        }
        return instance;
    }


    public void saveString(String key,String value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void saveInt(String key,int value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void saveBoolean(String key,boolean value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    //get value from key
    public String getStringValue(String key, String defaultValue){
        return sharedPreferences.getString(key, defaultValue);
    }

    public int getIntValue(String key, int defaultValue){
        return sharedPreferences.getInt(key, defaultValue);
    }

    public boolean getbooleanValue(String key, boolean defaultValue){
        return sharedPreferences.getBoolean(key, defaultValue);
    }
}
