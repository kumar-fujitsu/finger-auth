package com.example.fingerauth;

import android.content.Context;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.util.Base64;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;

public class CommonUtil {

    private static final String TAG = "Common Util";
    /**
     * Function to generate key
     */
    public static void generateKey(KeyStore keyStore,String keyName) {

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.
                    getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("failed to generate key", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new KeyGenParameterSpec.Builder(
                    keyName, KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());

            keyGenerator.generateKey();

        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getEncryptedData(Context mContext,String key_alias,String data){
        SecretKey key = getKey(key_alias);
        String encryptedData = null;
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" +
                    KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);

            cipher.init(Cipher.ENCRYPT_MODE, key);
            final byte[] ivBytes = cipher.getIV();
            final byte[] encryptedBytes = cipher.doFinal(data.getBytes("UTF-8"));
            PreferenceUtil  preferenceUtil = PreferenceUtil.getInstance(mContext);
            preferenceUtil.saveString("iv",ivBytes.toString());
            encryptedData = Base64.encodeToString(encryptedBytes,Base64.DEFAULT);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return encryptedData;
    }

    private static SecretKey getKey(String alias){
        KeyStore keyStore = null;
        SecretKey key = null;
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        try {
            keyStore.load(null);
            KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry) keyStore.getEntry(alias,null);
            key = secretKeyEntry.getSecretKey();
        } catch (KeyStoreException |
                CertificateException |
                UnrecoverableKeyException |
                IOException | NoSuchAlgorithmException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        }
        return key;
    }

    public static String getDecryptedData(Context mContext,String alias,String encryptedData){
        SecretKey key = getKey(alias);
        byte[] decryptedData = null;
        Cipher cipher;
        PreferenceUtil  preferenceUtil = PreferenceUtil.getInstance(mContext);
        String encryptionIv = preferenceUtil.getStringValue("iv",null);
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" +
                    KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);

            final GCMParameterSpec spec = new GCMParameterSpec(128, encryptionIv.getBytes());
            cipher.init(Cipher.DECRYPT_MODE, key, spec);
            byte[] encryptedByte = Base64.decode(encryptedData, Base64.DEFAULT);
            decryptedData = cipher.doFinal(encryptedByte);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        //cipher = Cipher.getInstance("AES/GCM/NoPadding");
        return Base64.encodeToString(decryptedData,Base64.DEFAULT);
    }
}
