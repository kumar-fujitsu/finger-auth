package com.example.fingerauth;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

public class FingerPrintHandler extends FingerprintManager.AuthenticationCallback {

    private Context mContext;
    private FingerAuthentication fingerAuthentication;

    public FingerPrintHandler(Context context,FingerAuthentication authentication){
        mContext = context;
        fingerAuthentication = authentication;
    }

    public void startAuthentication(FingerprintManager manager,
                                    FingerprintManager.CryptoObject cryptoObject){
        CancellationSignal signal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, signal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        this.update("Fingerprint Authentication error.\n"+errString, false);
    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        this.update("Fingerprint Authentication help.\n"+helpString, false);
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        this.update("Fingerprint Authentication succeeded.", true);
    }

    @Override
    public void onAuthenticationFailed() {
        this.update("Fingerprint Authentication failed.", false);
    }

    public void update(String e, Boolean success){
        fingerAuthentication.onResult(success);
        /*TextView textView = (TextView) ((Activity)mContext).findViewById(R.id.errorText);
        textView.setText(e);
        if(success){
            textView.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimaryDark));
        }*/
    }
}
