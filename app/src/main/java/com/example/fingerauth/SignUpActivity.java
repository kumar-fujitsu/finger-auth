package com.example.fingerauth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.security.KeyStore;

public class SignUpActivity extends AppCompatActivity {

    EditText edtPassword;
    Button btnSignUp;
    KeyStore keyStore;
    TextView tvLogin;
    private static final String TAG = "Sign Up Activity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activiity_sign_up);
        edtPassword = findViewById(R.id.editText5);
        btnSignUp = findViewById(R.id.button);
        tvLogin = findViewById(R.id.textView2);

        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // after successful validation call server
                // on success of api save user id and encrypted password
                CommonUtil.generateKey(keyStore,ConstantUtil.KEY_PSW);
                saveDataOnSuccess(ConstantUtil.LOGIN_EMAIL,ConstantUtil.LOGIN_PASSWORD);

            }
        });

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callNextActivity(true);
            }
        });
    }

    private void saveDataOnSuccess(String userId,String password){
        String encryptedData = CommonUtil.getEncryptedData(SignUpActivity.this,ConstantUtil.KEY_PSW,password);
        PreferenceUtil  preferenceUtil = PreferenceUtil.getInstance(this);
        preferenceUtil.saveString("password",encryptedData);
        preferenceUtil.saveString("user_id",userId);
        callNextActivity(false);
    }

    private void callNextActivity(Boolean isBackPress) {
        if (isBackPress){
            Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
            startActivity(intent);
        }else {
            Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
            startActivity(intent);
        }
        finish();
    }
}
