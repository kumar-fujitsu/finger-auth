package com.example.fingerauth;

public interface FingerAuthentication {
    void onResult(boolean value);
}
