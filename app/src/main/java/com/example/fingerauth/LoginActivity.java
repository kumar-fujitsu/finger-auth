package com.example.fingerauth;

import android.Manifest;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import static com.example.fingerauth.ConstantUtil.LOGIN_EMAIL;
import static com.example.fingerauth.ConstantUtil.LOGIN_PASSWORD;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, FingerAuthentication {

    private EditText edtEmail, edtPassword;
    private Button btnLogin;
    private KeyStore keyStore;
    private static final String TAG = "FingerPrint Activity";
    // variable name for storing key in keystore container
    private static final String KEY_NAME = "kumarauth";
    private Cipher cipher;
    private KeyguardManager keyguardManager;
    private FingerprintManager fingerprintManager;
    private PreferenceUtil preferenceUtil;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);
        btnLogin = findViewById(R.id.btn_sign_in);

        btnLogin.setOnClickListener(this);

        // Initializing both Android Keyguard Manager and Fingerprint Manager
        keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        fingerprintManager =
                (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

        preferenceUtil = PreferenceUtil.getInstance(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_sign_in) {
            if (validate(edtEmail.getText().toString().trim(),
                    edtPassword.getText().toString().trim())) {
                if (preferenceUtil.getbooleanValue("FINGER_AUTH_ENABLE", false)) {
                    callNextActivity();
                } else {
                    showDialog();
                }

            } else {
                Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (preferenceUtil.getbooleanValue("FINGER_AUTH_ENABLE", false)) {
            Intent intent = new Intent(LoginActivity.this, FingerPrintActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private boolean validate(String email, String password) {
        return LOGIN_EMAIL.equalsIgnoreCase(email) && LOGIN_PASSWORD.equalsIgnoreCase(password);
    }

    private void callNextActivity() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    // Check whether the device has a Fingerprint sensor.
    private boolean checkFingerSensor() {
        if (!fingerprintManager.isHardwareDetected()) {
            Toast.makeText(this, "Your Device does not have a Fingerprint Sensor",
                    Toast.LENGTH_SHORT).show();
            return false;
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) !=
                PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Fingerprint authentication permission not enabled",
                    Toast.LENGTH_SHORT).show();
            return false;
        }

        // Check whether at least one fingerprint is registered
        if (!fingerprintManager.hasEnrolledFingerprints()) {
            Toast.makeText(this, "Register at least one fingerprint in Settings",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        // Checks whether lock screen security is enabled or not
        if (!keyguardManager.isKeyguardSecure()) {
            Toast.makeText(this, "Lock screen security not enabled in Settings",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    /**
     * Function to generate key
     */
    private void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.
                    getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("failed to generate key", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new KeyGenParameterSpec.Builder(
                    KEY_NAME, KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CTR)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());

            keyGenerator.generateKey();

        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" +
                    KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }


        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException |
                CertificateException |
                UnrecoverableKeyException |
                IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    private void loginThroughFingerAuth() {
        if (checkFingerSensor()){
            generateKey();

            if (cipherInit()) {
                FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                FingerPrintHandler helper = new FingerPrintHandler(this, this);
                helper.startAuthentication(fingerprintManager, cryptoObject);
            }
        }else {

        }
    }

    @Override
    public void onResult(boolean value) {
        if (value){
            credentialValidation();
        }
    }

    private void showDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_finger_auth);
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);

        Button btnEnable = dialog.findViewById(R.id.btn_enable);


        btnEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                preferenceUtil.saveBoolean("FINGER_AUTH_ENABLE", true);
               Intent intent = new Intent(LoginActivity.this,FingerPrintActivity.class);
               startActivity(intent);
               LoginActivity.this.finish();
            }
        });

        dialog.show();
    }

    private void credentialValidation(){
        PreferenceUtil preferenceUtil = PreferenceUtil.getInstance(this);
        String encryptedData = preferenceUtil.getStringValue("password",null);
        String userID = preferenceUtil.getStringValue("user_id",null);
        String decryptedData = CommonUtil.getDecryptedData(LoginActivity.this,
                ConstantUtil.KEY_PSW,encryptedData);

        if(validate(userID,decryptedData)){
            callNextActivity();
        }else {
            Toast.makeText(this,"Invalid Credential",Toast.LENGTH_SHORT).show();
        }
    }
}
